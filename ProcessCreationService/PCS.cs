﻿using RemoteServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;

namespace ProcessCreationService
{
    class PCS
    {
        static void Main(string[] args)
        {
            IDictionary properties = new Hashtable
            {
                ["name"] = "tcp:11000",
                ["port"] = "11000"
            };

            TcpChannel channel = new TcpChannel(properties, null, null);
            ChannelServices.RegisterChannel(channel, false);
            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(PCSService),
                "PCS",
                WellKnownObjectMode.Singleton);
            Console.ReadLine();
        }
    }
    class PCSService : MarshalByRefObject, IPCSServices
    {
        private Dictionary<string, string> processes = new Dictionary<string, string>();
        private Dictionary<string, Process> processesV2 = new Dictionary<string, Process>();
        public bool FreezeProcess(string pId)
        {
            try
            {
                ((IClientServices)Activator.GetObject(typeof(IClientServices), processes[pId])).SignalFreeze();
                Console.WriteLine("Froze process {0}", pId);
                return true;
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Process {0} doesn't exist", pId);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error freezing process {0}: {1}", pId, e.Message);
                return false;
            }
        }
        public bool UnFreezeProcess(string pId)
        {
            try
            {
                ((IClientServices)Activator.GetObject(typeof(IClientServices), processes[pId])).SignalUnFreeze();
                Console.WriteLine("UnFroze process {0}", pId);
                return true;
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Process {0} doesn't exist", pId);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error freezing process {0}: {1}", pId, e.Message);
                return false;
            }
        }

        public bool StartClient(string pId, string clientURL, string serverURL, string mSecRound, string numPlayers, string filename)
        {
            Console.WriteLine("Starting client...");
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = Environment.CurrentDirectory + "/../../../Client/bin/Debug/Client.exe";
                process.StartInfo.Arguments = String.Join(" ", pId, clientURL, serverURL, mSecRound, numPlayers);
                if (filename.Length > 0)
                    process.StartInfo.Arguments += " " + filename;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                process.StartInfo.WorkingDirectory = Environment.CurrentDirectory + "/../../../Client/bin/Debug";
                process.Start();
                processes.Add(pId, clientURL);
                processesV2.Add(pId, process);
                Console.WriteLine("Client started successefully.");
                return true;
            }
            catch (Exception e)
            {
                //TODO: Handle exception
                Console.WriteLine("Error starting client.");
                Console.WriteLine(e);
                return false;
            }


        }

        public bool StartServer(string pId, string serverURL, string mSecRound, string numPlayers)
        {
            try
            {
                Console.WriteLine("Starting Server on this machine...");
                Process process = new Process();
                process.StartInfo.FileName = Environment.CurrentDirectory + "/../../../Server/bin/Debug/Server.exe";
                process.StartInfo.Arguments = String.Format("{0} {1} {2} {3} {4}",
                    pId, serverURL, mSecRound, numPlayers, serverURL);
                process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                process.StartInfo.WorkingDirectory = Environment.CurrentDirectory + "/../../../Server/bin/Debug";
                process.Start();
                processes.Add(pId, serverURL);
                processesV2.Add(pId, process);
                Console.WriteLine("Server started successefully.");
                return true;
            }
            catch (Exception e)
            {
                //TODO: Handle exception
                Console.WriteLine("Error starting server.");
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool StartReplica(string pId, string serverURL, string mSecRound, string numPlayers, string primaryURL)
        {
            try
            {
                Console.WriteLine("Starting Server Replica on this machine...");
                Process process = new Process();
                process.StartInfo.FileName = Environment.CurrentDirectory + "/../../../Server/bin/Debug/Server.exe";
                process.StartInfo.Arguments = String.Format("{0} {1} {2} {3} {4}",
                    pId, serverURL, mSecRound, numPlayers, primaryURL);
                process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                process.StartInfo.WorkingDirectory = Environment.CurrentDirectory + "/../../../Server/bin/Debug";
                process.Start();
                processes.Add(pId, serverURL);
                processesV2.Add(pId, process);
                Console.WriteLine("Server started successefully.");
                return true;
            }
            catch (Exception e)
            {
                //TODO: Handle exception
                Console.WriteLine("Error starting server.");
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public void Exit()
        {
            Console.WriteLine("Exiting");
            foreach (string pId in processes.Keys)
            {
                CrashProcess(pId);
            }

            Environment.Exit(0);
        }

        public bool CrashProcess(string pId)
        {
            try
            {
                processesV2[pId].Kill();
                Console.WriteLine("Crashed process " + pId);
                return true;
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Process {0} doesn't exist", pId);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error crashing process {0}: {1}", pId, e.Message);
                return false;
            }
        }

        public bool InjectDelay(string srcPID, string descPID)
        {
            try
            {
                ((IClientServices)Activator.GetObject(typeof(IClientServices), processes[srcPID])).SignalDelay(descPID);
                return true;
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Process {0} doesn't exist", srcPID);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error applying delay between process {0} & {1}: {2}",
                    srcPID, descPID, e.Message);
                return false;
            }
        }

        public bool LocalState(string pId, int roundNumber)
        {
            try
            {
                ((IClientServices)Activator.GetObject(typeof(IClientServices), processes[pId])).LocalState(roundNumber);
                return true;
            }
            catch (KeyNotFoundException)
            {
                Console.WriteLine("Process {0} doesn't exist", pId);
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getting local state of {0}: {1}", pId, e.Message);
                return false;
            }
        }
    }

}
